FROM php:7.2-fpm-jessie
RUN apt-get update -yqq && apt-get -y install gnupg git libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev && curl -sL https://deb.nodesource.com/setup_9.x | bash - && apt-get install nodejs && docker-php-ext-install pdo_mysql pdo_pgsql intl gd zip bz2 opcache && curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/bin/composer && chmod +x /usr/bin/composer && apt autoremove && apt clean all


